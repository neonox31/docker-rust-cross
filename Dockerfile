ARG RUST_VERSION=latest

FROM rust:$RUST_VERSION

ENV CROSS_DOCKER_IN_DOCKER=true

RUN curl -fsSL https://get.docker.com | sh
RUN cargo install --force cargo-make
RUN cargo install --force cross
